from dependency_injector import containers, providers
from infrastructure.gateways.local_image_gateway import LocalImageGateway
from infrastructure.image_repository import ImageRepository
from domain.meme_generator import MemeGenerator


class Container(containers.DeclarativeContainer):
    wiring_config = containers.WiringConfiguration(
        packages=["application.views"]
    )
    config = providers.Configuration(ini_files=["config.ini"])
    image_gateway = providers.Singleton(
        LocalImageGateway, path=config.image_repository.path
    )
    image_repository = providers.Singleton(
        ImageRepository, gateway=image_gateway
    )
    meme_generator = providers.Singleton(
        MemeGenerator, image_repository=image_repository
    )
