from infrastructure.gateways.gateway import ImageGateway
from PIL import Image


class LocalImageGateway(ImageGateway):
    def __init__(self, path: str):
        self.path = path

    def get_image(self, index: int) -> Image.Image:
        try:
            return Image.open(f"{self.path}/image{index}.png")
        except FileNotFoundError:
            return Image.open(f"{self.path}/image{index}.jpg")
