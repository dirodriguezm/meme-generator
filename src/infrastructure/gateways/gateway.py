from PIL import Image


class ImageGateway:
    def get_image(self, index: int) -> Image:
        raise NotImplementedError()
