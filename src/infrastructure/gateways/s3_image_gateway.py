from infrastructure.gateways.gateway import ImageGateway


class S3ImageGateway(ImageGateway):
    def __init__(self, path: str):
        self.path = path
