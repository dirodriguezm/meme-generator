import random
from PIL import Image
from infrastructure.gateways.gateway import ImageGateway


class ImageRepositoryInterface:
    def get_image():
        raise NotImplementedError()


class ImageRepository(ImageRepositoryInterface):
    def __init__(self, gateway: ImageGateway):
        self.gateway = gateway

    def get_image(self) -> Image.Image:
        r = random.randint(1, 10)
        return self.gateway.get_image(r)
