from flask import Flask
from containers.containers import Container
from application.views import meme_routes, root_route


def create_app() -> Flask:

    container = Container()
    app = Flask(__name__)
    app.container = container

    app.register_blueprint(meme_routes)
    app.register_blueprint(root_route)

    return app
