from flask import Blueprint, request, send_file
from domain.meme_generator import MemeGenerator
from io import BytesIO
from dependency_injector.wiring import inject, Provide
from containers.containers import Container

meme_routes = Blueprint("memes", __name__)


@meme_routes.route("/generate_meme")
@inject
def generate_meme(
    meme_generator: MemeGenerator = Provide[Container.meme_generator],
):
    args = request.args
    meme = meme_generator.create_meme(
        args.get("top_text"), args.get("bottom_text", "")
    )
    bio = BytesIO()
    meme.image.save(bio, "PNG", quality=100)
    bio.seek(0)
    return send_file(bio, mimetype="image/png")


root_route = Blueprint("root", __name__)


@root_route.route("/")
def hello():
    return "hello"
