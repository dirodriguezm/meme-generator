from PIL.Image import Image
from PIL import ImageDraw
from PIL import ImageFont


class Meme:
    def __init__(self, image: Image, top_text: str, bottom_text: str):
        self.image = image
        self.top_text = top_text
        self.bottom_text = bottom_text

    def split_text(self, text: str) -> str:
        if text == "":
            return text
        if not text:
            return ""
        text_arr = text.split(" ")
        max_words = int(self.image.width / 150)
        counter = 0
        final_text = ""
        for word in text_arr:
            final_text += word
            if counter == max_words:
                final_text += "\n"
                counter = 0
            else:
                final_text += " "
            counter += 1
        return final_text

    def generate_meme(self):
        imdraw = ImageDraw.Draw(self.image)
        font_size = int(self.image.height / 12)
        font = ImageFont.truetype(
            "/home/diego/Projects/meme-generator/assets/fonts/impact.ttf",
            font_size,
        )
        x, y = (10, 10)
        split_top = self.split_text(self.top_text)
        split_bot = self.split_text(self.bottom_text)
        imdraw.text((x - 1, y - 1), split_top, font=font, fill=(0, 0, 0))
        imdraw.text((x + 1, y - 1), split_top, font=font, fill=(0, 0, 0))
        imdraw.text((x - 1, y + 1), split_top, font=font, fill=(0, 0, 0))
        imdraw.text((x + 1, y + 1), split_top, font=font, fill=(0, 0, 0))
        imdraw.text(
            (x, y),
            self.split_text(self.top_text),
            fill=(255, 255, 255),
            font=font,
        )
        x, y = (10, self.image.height - 30)
        imdraw.text((x - 1, y - 1), split_bot, font=font, fill=(0, 0, 0))
        imdraw.text((x + 1, y - 1), split_bot, font=font, fill=(0, 0, 0))
        imdraw.text((x - 1, y + 1), split_bot, font=font, fill=(0, 0, 0))
        imdraw.text((x + 1, y + 1), split_bot, font=font, fill=(0, 0, 0))
        imdraw.text(
            (x, y),
            split_bot,
            fill=(255, 255, 255),
            font=font,
        )
