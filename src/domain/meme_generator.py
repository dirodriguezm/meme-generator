from infrastructure.image_repository import (
    ImageRepositoryInterface,
)
from PIL.Image import Image
from domain.entities.meme import Meme


class MemeGeneratorInterface:
    def get_image() -> Image:
        raise NotImplementedError()

    def create_meme(top_text: str, bottom_text: str) -> Meme:
        raise NotImplementedError()


class MemeGenerator(MemeGeneratorInterface):
    def __init__(self, image_repository: ImageRepositoryInterface):
        self.image_repository = image_repository

    def create_meme(self, top_text: str, bottom_text: str) -> Meme:
        img = self.image_repository.get_image()
        meme = Meme(img, top_text, bottom_text)
        meme.generate_meme()
        return meme
