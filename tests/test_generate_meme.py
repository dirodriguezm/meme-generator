import pytest
from src.application.app import create_app
from mocks.image_gateway_mock import ImageGatewayMock


@pytest.fixture
def app():
    app = create_app()
    app.config.update(
        {
            "TESTING": True,
        }
    )
    yield app
    app.container.unwire()


@pytest.fixture()
def client(app):
    return app.test_client()


def test_generate_meme(client, app):
    gateway_mock = ImageGatewayMock("SUCCESS")
    with app.container.image_gateway.override(gateway_mock):
        response = client.get("/generate_meme")

    assert response.status_code == 200


def test_generate_meme_gateway_error(client, app):
    gateway_mock = ImageGatewayMock("FAILURE")
    with app.container.image_gateway.override(gateway_mock):
        with pytest.raises(Exception) as e:
            response = client.get("/generate_meme?top_text=test")
            assert response.status_code == 500
            assert str(e) == "Could not retrieve image"
