from src.infrastructure.gateways.gateway import ImageGateway
from PIL import Image


class ImageGatewayMock(ImageGateway):
    def __init__(self, test_type="SUCCESS"):
        self.test_type = test_type

    def get_image(self, index: int):
        if self.test_type == "SUCCESS":
            return Image.open("assets/local_images/image1.png")
        elif self.test_type == "FAILURE":
            raise Exception("Could not retrieve image")
